ifneq ($(KERNELRELEASE),)

# kbuild part of makefile
obj-m  := tcp_probe.o
#tcp_probe-y := tcp_probe.o

else
# normal makefile

KDIR ?= /lib/modules/`uname -r`/build

default:
	$(MAKE) -C $(KDIR) M=$$PWD

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
endif

