To use this module you need to change the kernel.

1. in linux-4.14.161/include/uapi/linux add the attached file inet_diag.h or add the modify tcp_bbr_info struct as given below:
```
struct tcp_bbr_info {
        /* u64 bw: max-filtered BW (app throughput) estimate in Byte per sec: */
        __u32   bbr_bw_lo;              /* lower 32 bits of bw */
        __u32   bbr_bw_hi;              /* upper 32 bits of bw */
        __u32   bbr_min_rtt;            /* min-filtered RTT in uSec */
        __u32   bbr_pacing_gain;        /* pacing gain shifted left 8 bits */
        __u32   bbr_cwnd_gain;          /* cwnd gain shifted left 8 bits */
        __u32   bbr_mode;               /*sandy*/
        __u32   bbr_lt_use_bw;          /*sandy*/
        __u32   bbr_lt_bw;              /*sandy*/
        __u32   bbr_cycle_idx;          /*sandy*/
        __u32   bbr_full_bw;            /*sandy*/
};
```
2. Now modify linux-4.14.161/net/ipv4/tcp_bbr.cc file or use the one in repo. Usually you add sandy commented lines below into bbr_get_info 
```
static size_t bbr_get_info(struct sock *sk, u32 ext, int *attr,
                           union tcp_cc_info *info)
{
        if (ext & (1 << (INET_DIAG_BBRINFO - 1)) ||
            ext & (1 << (INET_DIAG_VEGASINFO - 1))) {
                struct tcp_sock *tp = tcp_sk(sk);
                struct bbr *bbr = inet_csk_ca(sk);
                u64 bw = bbr_bw(sk);

                bw = bw * tp->mss_cache * USEC_PER_SEC >> BW_SCALE;
                memset(&info->bbr, 0, sizeof(info->bbr));
                info->bbr.bbr_bw_lo             = (u32)bw;
                info->bbr.bbr_bw_hi             = (u32)(bw >> 32);
                info->bbr.bbr_min_rtt           = bbr->min_rtt_us;
                info->bbr.bbr_pacing_gain       = bbr->pacing_gain;
                info->bbr.bbr_cwnd_gain         = bbr->cwnd_gain;
                /*sandy*/
                info->bbr.bbr_mode              = bbr->mode;
                info->bbr.bbr_lt_use_bw         = bbr->lt_use_bw;
                info->bbr.bbr_lt_bw             = bbr->lt_bw;
                info->bbr.bbr_cycle_idx         = bbr->cycle_idx;
                info->bbr.bbr_full_bw           = bbr->full_bw;
                /*sandy*/
                *attr = INET_DIAG_BBRINFO;
                return sizeof(info->bbr);
        }
        return 0;
}
```
After that compile and load the kernel image and then use the tcp_probe module. This module is trying to collect the BBR alogirthm related information with addition tcp congestion control variables.

Sandesh
